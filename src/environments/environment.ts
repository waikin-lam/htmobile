// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyC1ocU1UombGNxa0z5jED2suB1oklgQ4EQ",
    authDomain: "htapp-94cdb.firebaseapp.com",
    databaseURL: "https://htapp-94cdb.firebaseio.com",
    projectId: "htapp-94cdb",
    storageBucket: "htapp-94cdb.appspot.com",
    messagingSenderId: "557439179986"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
