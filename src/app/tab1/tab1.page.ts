import { Component, OnInit } from '@angular/core';
import { NavController, Platform } from '@ionic/angular';
import { Router, NavigationExtras } from '@angular/router';

import { Observable, combineLatest } from 'rxjs';
import { map } from 'rxjs/operators';
import { Task } from '../../app/models/task.interface';
import { FirestoreService } from '../../app/firestore.service';
import { AuthService } from '../../app/auth.service';
import { AngularFirestore } from '@angular/fire/firestore';
import * as moment from 'moment-timezone';
import * as firebase from 'firebase/app';

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page implements OnInit {
  momentjs: any = moment;
  taskList: Observable<any>;
  tasks: [];
  id: [];

  constructor(public navCtrl: NavController, private firestoreProvider: FirestoreService, private platform: Platform, public authProvider: AuthService, private fireStore: AngularFirestore, private router: Router) {}

  async ngOnInit() {
    var user = firebase.auth().currentUser;

    // get today's date
    var currentDate = new Date().toISOString().slice(0,10);
    console.log(currentDate);

    // get date GMT+1 to standardize with firestore
    var fsDate = moment.tz(currentDate, "America/Anchorage").format();
    var cutOffDate = moment(fsDate).subtract(4, "days");
    var start = moment.tz(cutOffDate, "America/Anchorage").format()
    //console.log(start);
    //console.log(fsDate);

    if (user) {
      var email = user.email;
      var x = email.indexOf('.');
      var id = email.slice(0,x).toUpperCase();
      //console.log(id);
      this.getQuery(id, start).subscribe(res => {
        this.tasks = res;
        console.log(this.tasks);
      })
      //this.taskList = this.fireStore.collection("Trucks").doc(id).collection("DOs", ref => ref.where('start', '>=', '2019-08-05T00:00:00-08:00')).valueChanges(); // to change time
      /*this.taskList.subscribe(res => {
        this.tasks = res;
        console.log(this.tasks);
      })*/
    }
  }

  logoutUser() {
    this.authProvider.logoutUser()
    .then(res => {
      //console.log(res);
      this.navCtrl.navigateRoot('/login');
    })
    .catch(error => {
      console.log(error);
    })
  }

  goToDOPage(event, task) {
    var user = firebase.auth().currentUser;
    var email = user.email;
    var x = email.indexOf('.');
    var id = email.slice(0,x).toUpperCase();
    //this.navCtrl.navigateForward('do', {task:task});
    let navigationExtras: NavigationExtras = {
      state: {
        task: task,
        id: id,
      }
    };
    //console.log(navigationExtras);
    this.router.navigate(['/do'], navigationExtras);
    //this.router.navigate(['/do', task]);
  }

  getQuery(id, start) {

    console.log(id);
    console.log(start);
    // to amend start time
    const $one = this.fireStore.collection("Trucks").doc(id).collection("DOs", ref => ref.where('start', '>=', start).where('complete', '==', "false")).valueChanges();
    const $two = this.fireStore.collection("Trucks").doc(id).collection("DOs", ref => ref.where('start', '>=', start).where('complete', "array-contains", "false")).valueChanges();
    //const $three = this.fireStore.collection("Trucks").doc(id).collection("DOs", ref => ref.where('end', '>=', start).where('complete', '==', "false")).valueChanges();
    //const $four = this.fireStore.collection("Trucks").doc(id).collection("DOs", ref => ref.where('end', '>=', start).where('complete', "array-contains", "false")).valueChanges();
    //const $three = this.fireStore.collection("Trucks").doc(id).collection("DOs", ref => ref.where('complete', "array-contains", "false")).valueChanges();
    const combinedList = combineLatest<any[]>($one, $two).pipe(map(arr => arr.reduce((acc,cur) => acc.concat(cur))));
    combinedList.subscribe(res => {
      console.log(res)
    });
    return combinedList;
    /*return combined($one, $two).pipe(
      map(([one, two]) => [...one, ...two])
    )*/
  }
}
