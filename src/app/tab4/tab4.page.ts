import { Component, OnInit } from '@angular/core';
import { NavController, Platform } from '@ionic/angular';
import { GoogleMaps, GoogleMap, GoogleMapsEvent, GoogleMapOptions, Marker, Environment } from '@ionic-native/google-maps/ngx';

import { Observable } from 'rxjs';
import { Trailer } from '../../app/models/trailer.interface';
import { FirestoreService } from '../../app/firestore.service';
import { AuthService } from '../../app/auth.service';

@Component({
  selector: 'app-tab4',
  templateUrl: 'tab4.page.html',
  styleUrls: ['tab4.page.scss']
})
export class Tab4Page implements OnInit {
  trailerList: Observable<any>;
  trailers: Trailer[];
  map: GoogleMap;
  constructor(public navCtrl: NavController, private firestoreProvider: FirestoreService, private platform: Platform, public authProvider: AuthService) {}

  //ionViewDidLoad() {
    //this.loadMap();
  //}
  async ngOnInit() {
    await this.platform.ready();
    await this.loadMap();
  }

  loadMap() {
    // This code is necessary for browser
    Environment.setEnv({
      'API_KEY_FOR_BROWSER_RELEASE': 'AIzaSyDmZXDrKFr3Ry4-PJitxuWEZFLvD0S1UTI',
      'API_KEY_FOR_BROWSER_DEBUG': 'AIzaSyDmZXDrKFr3Ry4-PJitxuWEZFLvD0S1UTI'
    });

    let mapOptions: GoogleMapOptions = {
    camera: {
       target: {
         lat: 2.083079,
         lng: 111.538642
       },
       zoom: 8,
       tilt: 30
     }
    };

    this.map = GoogleMaps.create('map_canvas', mapOptions);

    this.trailerList = this.firestoreProvider.getTrailerList().valueChanges();
    this.trailerList.subscribe(res=>{
      this.trailers = res;
      console.log(this.trailers); //array
      console.log(this.trailers[1].location); //geopoint
      console.log((this.trailers).length); //3

      var marker, i;

      // iterate through (this.trailers)
      for (i=0; i < (this.trailers).length; i++) {
          var trailerRef = this.trailers[i].code;
          var p = this.trailers[i].location;
          var geopoint = (Object.keys(p).map(key=>p[key]));
          let marker: Marker = this.map.addMarkerSync({
              title: trailerRef,
              icon: 'blue',
              animation: 'DROP',
              position: {
                  lat: geopoint[0],
                  lng: geopoint[1]
              }
          })
      }
    })

    let marker: Marker = this.map.addMarkerSync({
    title: 'Depot - Sarikei',
    icon: 'green',
    animation: 'DROP',
    position: {
      lat: 2.083079,
      lng: 111.538642
    }
    });

    let marker1: Marker = this.map.addMarkerSync({
    title: 'Depot - Kuching',
    icon: 'green',
    animation: 'DROP',
    position: {
      lat: 1.558244,
      lng: 110.385153
    }
    })

    marker.on(GoogleMapsEvent.MARKER_CLICK).subscribe(() => {
    //alert('HT Depot - Sarikei');
    });
  }

  logoutUser() {
    this.authProvider.logoutUser()
    .then(res => {
      //console.log(res);
      this.navCtrl.navigateRoot('/login');
    })
    .catch(error => {
      console.log(error);
    })
    //this.app.getRootNav().setRoot('MainPage');
  }
}
