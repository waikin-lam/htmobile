export interface Trailer {
  regNo: string;
  code: string;
  location: string;
}
