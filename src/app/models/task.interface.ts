export interface Task {
    cargo: string;
    complete: string;
    consignee: string;
    destination: string;
    end: string;
    sequence: string;
    start: string;
    type: string;
}
