import { Component } from '@angular/core';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { NavController, AlertController } from '@ionic/angular';
import { Observable } from 'rxjs';
import { AngularFirestore } from '@angular/fire/firestore';
import { Trailer } from '../../app/models/trailer.interface';
import { FirestoreService } from '../../app/firestore.service';
import * as firebase from 'firebase/app';
import { AuthService } from '../../app/auth.service';

@Component({
  selector: 'app-tab3',
  templateUrl: 'tab3.page.html',
  styleUrls: ['tab3.page.scss']
})
export class Tab3Page {

  constructor(public navCtrl: NavController, private geolocation: Geolocation, private firestoreProvider: FirestoreService, public alertCtrl: AlertController, afs: AngularFirestore, private authProvider: AuthService ) {}

  buttonClick(event): void {
    var code = event.srcElement.textContent;
    console.log(code);

    this.geolocation.getCurrentPosition().then((resp) =>
    {
        var latitude = resp.coords.latitude;
        var longitude = resp.coords.longitude;
        console.log(latitude);
        console.log(longitude);

        var locationData = new firebase.firestore.GeoPoint(latitude, longitude);
        console.log(locationData);

        //const confirmation = await this.presentAlert();

        const alert = this.alertCtrl.create({
          header: code,
          message: 'Do you want to update the location of this trailer?',
          buttons: [
            {
              text: 'No',
              role: 'cancel',
              handler: () => {
                console.log('No clicked');
              }
            },
            {
              text: 'Yes',
              handler: () => {
                console.log('Yes clicked');
                // update firestore
                this.firestoreProvider.getTrailerList().doc(code).update({location: locationData}).catch(error => console.log(error));

                const alert = this.alertCtrl.create({
                  header: code,
                  subHeader: 'Location Updated',
                  buttons: ['Ok']
                }).then(alert => alert.present());
              }
            }
          ]
        }).then(alert => alert.present());
    })
  }

  logoutUser() {
    this.authProvider.logoutUser()
    .then(res => {
      //console.log(res);
      this.navCtrl.navigateRoot('/login');
    })
    .catch(error => {
      console.log(error);
    })
    //this.app.getRootNav().setRoot('MainPage');
  }

}
