import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AlertController, NavController } from '@ionic/angular';
import { SignaturePad } from 'angular2-signaturepad/signature-pad';
import { AngularFireStorage } from '@angular/fire/storage';
import { AngularFirestore, AngularFirestoreDocument } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { FormsModule } from '@angular/forms';

export interface Item { name: string; }

@Component({
  selector: 'app-do',
  templateUrl: './do.page.html',
  styleUrls: ['./do.page.scss'],
})
export class DoPage implements OnInit {
  object: any;
  task: any;
  jobs: any;
  id: any;
  private itemDoc: AngularFirestoreDocument<Item>;
  item: Observable<Item>;

  @ViewChild(SignaturePad, {static: false}) signaturePad: SignaturePad;

  public signaturePadOptions: Object = {
    'minWidth': 5,
    'canvasWidth': 500,
    'canvasHeight': 300,
    'backgroundColor': "rgb(255,255,255)"
  };

  constructor(public navCtrl: NavController, private route: ActivatedRoute, private router: Router, private afs: AngularFireStorage, public alertCtrl: AlertController, private firestore: AngularFirestore) {

  }

  ngOnInit() {
    this.route.queryParams.subscribe(params => {
      if (this.router.getCurrentNavigation().extras.state) {
        this.object = this.router.getCurrentNavigation().extras.state;
        this.task = this.object.task;
        this.id = this.object.id;
        //this.id = this.router.getCurrentNavigation().extra.state.id;
        //console.log(this.task.task);
      }
    });
  }

  ngAfterViewInit() {
    this.signaturePad.set('minWidth', 5);
    this.signaturePad.clear();
  }

  /*drawComplete() {
    console.log(this.signaturePad.toDataURL("image/jpeg"));
  }*/

  /*drawStart() {
    console.log('begin drawing');
  }*/

  clearPad() {
    this.signaturePad.clear();
  }

  saveSignature(task, id) {
    console.log(this.task.signatory); // name
    //update firestore
    this.itemDoc = this.firestore.doc<Item>(`Trucks/${id}/`);
    //this.itemDoc = this.firestore.doc<Item>('/trucks/');
    //this.tasks = this.itemDoc.snapshotChanges();
    //this.tasks = this.itemDoc.collection<Item>('DOs').snapshotChanges();
    this.jobs = this.itemDoc.collection<Item>('DOs').valueChanges({idField: 'eventId'});
    this.jobs.subscribe(res => {
      console.log(res);
      if (task.container) {
        //console.log(task.container);
        //console.log(res.length);
        for (var i=0; i < res.length; i++) {
          var row = res[i].row;
          if (task.row == row) {
            var title = res[i].eventId;
            this.firestore.doc(`Trucks/${id}/DOs/${title}`).update({"complete": "true"});
            var base64 = this.signaturePad.toDataURL();
            this.firestore.doc(`Trucks/${id}/DOs/${title}`).update({"sign": base64});
            this.firestore.doc(`Trucks/${id}/DOs/${title}`).update({"signatory": this.task.signatory});
          }
        }
      } else if (task.cargo) {
        for (var i=0; i < res.length; i++) {
          var row = res[i].row;
          if (task.row == row) {
            var title = res[i].eventId;
            this.firestore.doc(`Trucks/${id}/DOs/${title}`).update({"complete": "true"});
            var base64 = this.signaturePad.toDataURL();
            this.firestore.doc(`Trucks/${id}/DOs/${title}`).update({"sign": base64});
            this.firestore.doc(`Trucks/${id}/DOs/${title}`).update({"signatory": this.task.signatory});
          }
        }
      }
    });
    //var base64 = this.signaturePad.toDataURL("/" + task.type + "/" + task.row);
    //var ref = this.afs.ref("/" + task.type + "/" + task.row);
    //var saveTask = ref.putString(base64);
    console.log("base64 string saved");
    // to also update firestore
    const alert = this.alertCtrl.create({
       header: task.consignee,
       subHeader: 'D/O Acknowledged',
       buttons: ['Ok']
   }).then(
     alert => {
       alert.present();
       this.navCtrl.navigateRoot('/tabs');
   });
  }

}
