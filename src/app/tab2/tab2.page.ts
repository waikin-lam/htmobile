import { Component } from '@angular/core';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { QRScanner, QRScannerStatus } from '@ionic-native/qr-scanner/ngx';
import { AndroidPermissions } from '@ionic-native/android-permissions/ngx';
import { NavController, AlertController } from '@ionic/angular';
import { Observable } from 'rxjs';
import { AngularFirestore } from '@angular/fire/firestore';
import { Trailer } from '../../app/models/trailer.interface';
import { FirestoreService } from '../../app/firestore.service';
import * as firebase from 'firebase/app';
import { AuthService } from '../../app/auth.service';

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})
export class Tab2Page {

  constructor(public navCtrl: NavController, private geolocation: Geolocation, private qrScanner: QRScanner, private androidPermissions: AndroidPermissions, private alertCtrl: AlertController, private firestoreProvider: FirestoreService, private afs: AngularFirestore, private authProvider: AuthService ) {}

  ionViewWillEnter() {
    this.showCamera();
    console.log("Open camera");
    // Optionally request the permission early
    this.qrScanner.prepare()
      .then((status: QRScannerStatus) => {
         if (status.authorized) {
           // camera permission was granted
           console.log("permission granted");

           // start scanning
           let scanSub = this.qrScanner.scan().subscribe((code: string) => {
             console.log('Scanned something', code);

             this.geolocation.getCurrentPosition().then((resp) => {
                var latitude = resp.coords.latitude;
                var longitude = resp.coords.longitude;
                console.log(latitude);
                console.log(longitude);

                var locationData = new firebase.firestore.GeoPoint(latitude, longitude);
                console.log(locationData);

                // update firestore
                this.firestoreProvider.getTrailerList().doc(code).update({location: locationData}).catch(error => console.log(error));

                 const alert = this.alertCtrl.create({
                    header: code,
                    subHeader: 'Location Updated',
                    buttons: ['Ok']
                }).then(alert => alert.present());
             }).catch((error) => {
                const alert = this.alertCtrl.create({
                  header: 'Error',
                  subHeader: error,
                  buttons: ['Ok']
                }).then(alert => alert.present());
                console.log('Error getting location', error);
             })


             this.qrScanner.hide(); // hide camera preview
             scanSub.unsubscribe(); // stop scanning
             //this.navCtrl.pop(); // go back

             // show camera preview
             this.qrScanner.show();
            });

         } else if (status.denied) {
           // camera permission was permanently denied
           // you must use QRScanner.openSettings() method to guide the user to the settings page
           // then they can grant the permission from there
           console.log("permission denied");
         } else {
           // permission was denied, but not permanently. You can ask for permission again at a later time.
         }
      })
      .catch((e: any) => console.log('Error is', e));
    }

    ionViewDidLeave() {
      this.hideCamera();
      this.qrScanner.destroy();
    }

    showCamera() {
      window.document.querySelector('ion-app').classList.add('cameraView');
    }

    hideCamera() {
      window.document.querySelector('ion-app').classList.remove('cameraView');
    }

    logoutUser() {
      this.authProvider.logoutUser()
      .then(res => {
        //console.log(res);
        this.navCtrl.navigateRoot('/login');
      })
      .catch(error => {
        console.log(error);
      })
      //this.app.getRootNav().setRoot('MainPage');
    }
}
