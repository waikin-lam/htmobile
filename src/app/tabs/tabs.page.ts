import { Component } from '@angular/core';
import { NavController } from '@ionic/angular';
import { AuthService } from '../../app/auth.service';

@Component({
  selector: 'app-tabs',
  templateUrl: 'tabs.page.html',
  styleUrls: ['tabs.page.scss']
})
export class TabsPage {
  toggleMap = false;
  toggleTask = true;

  constructor(private authProvider: AuthService, public navCtrl: NavController) {}

  ionViewWillEnter() {
    if(this.authProvider.userDetails()){
      //console.log(this.authProvider.userDetails().email);
      var email = this.authProvider.userDetails().email;
      if (email === 'wai-kin.lam@high-tackle.com' || email === 'calebwsy@high-tackle.com' || email === 'ruby.yeong@high-tackle.com' || email === 'yk.yong@high-tackle.com' || email === 'victoraga.ht@gmail.com' || email === 'liong@high-tackle.com' || email === 'josephling.ht@gmail.com') {
        this.toggleMap = true;
        this.toggleTask = false;
      }
      return true;
    } else {
      this.navCtrl.navigateRoot('/login');
    }
  }
}
