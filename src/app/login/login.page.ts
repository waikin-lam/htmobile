import { EmailValidator } from '../validators/email';
import { Component, OnInit } from '@angular/core';
import { AlertController, LoadingController, NavController } from '@ionic/angular';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { AuthService } from '../../app/auth.service';

import * as firebase from 'firebase/app';
import 'firebase/auth';
import { environment } from '../../environments/environment';

firebase.initializeApp(environment.firebase);

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  public loginForm: FormGroup;
  //public loading: Loading;

  constructor(public formBuilder: FormBuilder, public navCtrl: NavController, public loadingCtrl: LoadingController, public alertCtrl: AlertController, public authProvider: AuthService) {
    this.loginForm = formBuilder.group({
      email: ['', EmailValidator.isValid],
      password: ['', Validators.compose([Validators.minLength(6)])]
    });
  }

  ngOnInit() {
  }

  loginUser(): void {
    if (!this.loginForm.valid) {
      console.log(
        `Form is not valid yet, current value: ${this.loginForm.value}`
      );
    } else {
      const email = this.loginForm.value.email;
      const password = this.loginForm.value.password;
      console.log(email);
      console.log(password);

      this.authProvider.loginUser(email, password).then(
        authData => {
          console.log(email);
          if (email === 'wai-kin.lam@high-tackle.com' || email === 'calebwsy@high-tackle.com' || email === 'ruby.yeong@high-tackle.com' || email === 'yk.yong@high-tackle.com' || email === 'victoraga.ht@gmail.com' || email === 'liong@high-tackle.com' || email === 'josephling.ht@gmail.com') {
            this.navCtrl.navigateForward('/tabs/tab4')
          } else {
            this.navCtrl.navigateForward('/tabs/tab1');
          }
        },
        error => {
            const alert = this.alertCtrl.create({
              message: error.message,
              buttons: [{ text: 'Ok', role: 'cancel' }]
            }).then(alert => alert.present());
        });
      }
    }

    goToMain() {
      this.navCtrl.navigateRoot('/front');
    }
}
