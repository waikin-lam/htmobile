import { Component } from '@angular/core';

import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

//import firebase from 'firebase/app';
//import 'firebase/auth';
//import { environment } from '../environments/environment';
//import { FirebaseAuthentication } from '@ionic-native/firebase-authentication/ngx'

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {
  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    //private firebaseAuthentication: FirebaseAuthentication
  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  };


  //this.firebaseAuthentication.onAuthStateChanged(user)
    //.then((res: any) => console.log(res))
    //.catch((error: any) => console.error(error));

  //firebase.initializeApp(environment);

  /*const unsubscribe = firebase.auth().onAuthStateChanged(user => {
    if (!user) {
      this.navigateRoot = '/front';
      unsubscribe();
    } else if (user) {
      this.navigateRoot = '/tabs';
      unsubscribe();
    }
  })*/
}
