import { Injectable } from '@angular/core';
import * as firebase from 'firebase/app';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor() { }

  loginUser(email: string, password: string) {
    return new Promise<any>((resolve, reject) => {
      firebase.auth().signInWithEmailAndPassword(email, password).then(
        res => resolve(res),
        err => reject(err)
      )
    })
  }

  logoutUser() {
    return new Promise((resolve, reject) => {
      if (firebase.auth().currentUser) {
        firebase.auth().signOut().then(() => {
          console.log("Logout");
          resolve();
        }).catch((error) => {
          reject();
        });
      }
    })
  }

  userDetails() {
    return firebase.auth().currentUser;
  }

  /*loginUser(email: string, password: string): Promise<any> {
    return firebase.auth().signInWithEmailAndPassword(email, password);
  }

  logoutUser(): Promise<void> {
    const userId: string = firebase.auth().currentUser.uid;

    return firebase.auth().signOut();
  }*/
}
