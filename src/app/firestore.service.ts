import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AngularFirestore } from '@angular/fire/firestore';
import { AngularFirestoreCollection } from '@angular/fire/firestore';
import { Trailer } from '../app/models/trailer.interface';

@Injectable({
  providedIn: 'root'
})
export class FirestoreService {

  constructor(public http: HttpClient, public firestore: AngularFirestore) { }

  getTrailerList(): AngularFirestoreCollection<Trailer> {
    return this.firestore.collection(`Trailers`);
  };
}
