import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-front',
  templateUrl: './front.page.html',
  styleUrls: ['./front.page.scss'],
})
export class FrontPage implements OnInit {

  constructor(public navCtrl: NavController) { }

  ngOnInit() {
  }

  goToScanner() {
    this.navCtrl.navigateForward('/scanner');
  }

  goToSelect() {
    this.navCtrl.navigateForward('/select');
  }

  goToTabs() {
    this.navCtrl.navigateForward('/tabs');
  }

  goToTasks() {
    this.navCtrl.navigateForward('/tabs')
  }
}
